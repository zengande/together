﻿namespace Together.Extensions.Claims
{
    public class CurrentUserInfo
    {
        public string Avatar { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public int Gender { get; set; }
    }
}
