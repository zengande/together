﻿using Together.Activity.Domain.SeedWork;

namespace Together.Activity.Domain.AggregatesModel.CategoryAggregate
{
    public interface ICategoryRepository
        : IRepository<Category>
    {
    }
}
