﻿using System.Collections.Generic;

namespace Together.Activity.API.Applications.IntegrationEvents.Events
{
    public class ActivityRecruitmentCompletedIntegrationEvent
    {
        public List<int> ActivityIds { get; set; }
    }
}
