﻿namespace Together.Notice
{
    public class Constants
    {
        public const string EmailNoticeEventName = "Together.Notice.Email";
        public const string ConfirmEmailAddressEventName = "Together.Notice.Email.Confirm.EmailAddress";

        public const string NoticeRecordRedisKey = "NoticeRecords";
    }
}
