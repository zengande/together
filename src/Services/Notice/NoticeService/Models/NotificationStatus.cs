﻿namespace Together.Notice.Models
{
    public enum NotificationStatus
    {
        Unknown = 0,
        Success = 1,
        Failure = 2
    }
}
