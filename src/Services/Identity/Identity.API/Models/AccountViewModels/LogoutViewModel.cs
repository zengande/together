﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Together.Identity.API.Models.AccountViewModels
{
    public class LogoutViewModel
    {
        public string LogoutId { get; set; }
    }
}
