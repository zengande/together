﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Location.API
{
    public class LocationsSettings
    {
        public string MongoConnentionString { get; set; }
        public string LocationsDatabaseName { get; set; }
    }
}
