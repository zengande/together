﻿using Microsoft.AspNetCore.Mvc;

namespace ManagePortal.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}