﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Together.ManagePortal
{
    public class AdminAuthorizationRequirement : AuthorizationHandler<AdminAuthorizationRequirement>, IAuthorizationRequirement
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminAuthorizationRequirement requirement)
        {
            var user = context.User;
            var roles = user.FindAll(JwtClaimTypes.Role);
            if (roles.Any(r => r.Value == "Administrator"))
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
