﻿namespace WebMVC.Infrastructure.Enums
{
    public enum ActivityStatus
    {
        Recruitment = 1,
        Processing = 2,
        Finished = 3,
        Cancelled = 5
    }
}
