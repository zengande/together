﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.ViewModels.Activity
{
    public class ActivityCategoryViewModel
    {
        public string Text { get; set; }
        public string ImgUrl { get; set; }
        public string Key { get; set; }
        public int Id { get; set; }
    }
}
