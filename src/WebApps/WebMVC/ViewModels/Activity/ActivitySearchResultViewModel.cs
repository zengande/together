﻿using WebMVC.Infrastructure.Dtos;

namespace WebMVC.ViewModels.Activity
{
    public class ActivitySearchResultViewModel
        : SearchResultBase<ActivitySearchResultDto>
    {
    }
}
